import numpy as np

# Homeworks
print("Homework #1")
a = np.random.randint(0, 10, 10)
print("Input: ")
print(a)
a = a[(a < 3) | (a > 8)]
print("Output: ")
print(a)

print("\nHomework #2")
b = np.random.randint(1, 10, 10)
print("Input: ", b)
print("Max: ", b.max())
b[b==b.max()]=0
print("Output: ", b)

print("\nHomework #3")
c = np.array([1, 3, 5, 6, 7])
print("First array: ", c)
d = np.array([3, 4, 6, 2])
print("Second array: ", d)
print("Common items: ", np.intersect1d(c, d))

print("\nHomework #4")
e = np.array(range(5))
print("Input: ", e)
print("Output: ", e[::-1])

print("\nHomework #5")
f =  np.arange(0, 9).reshape(3,3)
print("Output: ", f)

print("\nHomework #6")
g =  np.array([1,2,0,0,4,0])
print("Output: ", np.where(g == 0))

print("\nHomework #7")
h = np.random.randint(0, 100, size=(3, 3, 3))
print("Output: ", h)

print("\nHomework #8")
i = np.random.random(30)
print("Output: ", i)
print("The mean value: ", i.mean())

print("\nHomework #9")
j = np.ones((10,10))
j[1:-1,1:-1] = 0
print("Output: ", j)

print("\nHomework #10")
x = np.random.randint(0, 100, 20)
y = np.random.uniform(0, 20)
print("Input: ")
print("x = ", x)
print("y = ", y)
index = (np.abs(x-y)).argmin()
print("Index [", index, "] with value = ", x[index])